return {
  'spywhere/lightline-lsp',
  event = "VeryLazy",
  config = function()
    vim.g.lightline_lsp_loaded = true
  end
}
