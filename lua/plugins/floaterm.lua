return {
  'voldikss/vim-floaterm',
  event = "VeryLazy",
  config = function() require("config.floaterm") end
}
