return {
  'simnalamburt/vim-mundo',
  event = "VeryLazy",
  config = function()
    require("config.mundo")
  end
}
