return {
  bind = true,
  floating_window_above_cur_line = true,
  hint_enable = false,
  handler_opts = {
    border = "none"
  }
}
